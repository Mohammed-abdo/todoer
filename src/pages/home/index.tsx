import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import {
  todoSlice as todo,
  inProgressSlice as inProgress,
  doneSlice as done,
} from "../../modules/home/store/slice";
import { StoreState } from "../../modules/home/store";
import { IModel } from "../../modules/home/interfaces";
import {
  DoneColumn,
  InProgressColumn,
  ToDoColumn,
} from "../../modules/home/components";

type TAllSilces = "todo" | "inProgress" | "done";

export default function HomePage() {
  const dispatch = useDispatch();
  const appState = useSelector((state: StoreState) => state);

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) {
      return;
    }

    const { destination, source, draggableId } = result;
    const allSlices = { todo, inProgress, done };

    if (destination.droppableId === source.droppableId) {
      dispatch(
        allSlices[destination.droppableId as TAllSilces].actions.reorder(result)
      );
    } else {
      const [filterState] = (
        (appState as any)[source.droppableId] as IModel[]
      ).filter(({ id }) => id === draggableId);

      dispatch(
        allSlices[source.droppableId as TAllSilces].actions.remove(draggableId)
      );
      dispatch(
        allSlices[destination.droppableId as TAllSilces].actions.update({
          ...result,
          filterState,
        })
      );
    }
  };

  return (
    <>
      <Typography textAlign="left" variant="h3" mt={3} mb={5}>
        TODOer
      </Typography>
      <Grid container spacing={3} justifyContent="center">
        <DragDropContext onDragEnd={(res) => onDragEnd(res)}>
          <Grid item md={4}>
            <ToDoColumn />
          </Grid>
          <Grid item md={4}>
            <InProgressColumn />
          </Grid>
          <Grid item md={4}>
            <DoneColumn />
          </Grid>
        </DragDropContext>
      </Grid>
    </>
  );
}
