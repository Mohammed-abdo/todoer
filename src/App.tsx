import Container from "@mui/material/Container";
import { HomePage } from "./pages";

function App() {
  return (
    <Container>
      <HomePage />
    </Container>
  );
}

export default App;
