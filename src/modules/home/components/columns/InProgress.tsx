import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";
import { ColumnLayout } from "../../layouts";
import { StoreState } from "../../store";
import { inProgressSlice } from "../../store/slice/inProgress";

export default function InProgressColumn() {
  const { inProgress } = useSelector((state: StoreState) => state);

  const {
    actions: { completeStatus, remove, add, updateTextShowed },
  } = inProgressSlice;

  return (
    <>
      <Typography mb={3}>All inProgress tasks: {inProgress.length}</Typography>
      <ColumnLayout
        droppableId="inProgress"
        labelText="Type 'in progress' item"
        completedHandler={completeStatus}
        removeHandler={remove}
        addHandler={add}
        selectorState={inProgress}
        updateTextShowed={updateTextShowed}
      />
    </>
  );
}
